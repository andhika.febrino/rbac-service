package com.rbac.service;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.jboss.logging.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class UploadXLSParseService implements FileUploadParseService {

	private final Logger logger = Logger.getLogger(UploadXLSParseService.class);
	
	final ObjectMapper mapper = new ObjectMapper();
	
	@Override
	public String parseFile(InputStream file) {		
		List<String> data = new ArrayList<>();
		String dataResult = null;
        try (Workbook workbook = WorkbookFactory.create(file)) {
            Sheet sheet = workbook.getSheetAt(0);
            Iterator<Row> rowIterator = sheet.iterator();
            StringBuilder sb = new StringBuilder(); 
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                
                for (int i = 0; i < row.getLastCellNum(); i++) {
                    Cell cell = row.getCell(i, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
                   
                    sb.append(cell.toString());
                    sb.append(",");
                    
                }
                sb.setLength(sb.length()-1);
                data.add(sb.toString());
                sb.setLength(0);
                dataResult = mapper.writeValueAsString(data);
            }
        }catch(Exception e) {
        	logger.infof("Unable to parse XLS file");
        	return null;
        }
        
        return dataResult;
	}

}
