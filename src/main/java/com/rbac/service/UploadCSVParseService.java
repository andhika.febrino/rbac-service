package com.rbac.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class UploadCSVParseService implements FileUploadParseService {

	private final Logger logger = Logger.getLogger(UploadCSVParseService.class);

	final ObjectMapper mapper = new ObjectMapper();
	
	@Override
	public String parseFile(InputStream file) {
			
		String dataResult = null;
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(file))) {
			String line;
			List<String> data = new ArrayList<>();
			while ((line = reader.readLine()) != null) {
				data.add(line);
				logger.info("added" + line);
			}
			
			dataResult = mapper.writeValueAsString(data);
			logger.info("final result" +  dataResult);
		} catch (IOException e) {
			logger.info("error parsing csv");
			return dataResult;
		}

		return dataResult;
	}

}
