package com.rbac.service;

import org.jboss.logging.Logger;

import io.smallrye.reactive.messaging.annotations.Channel;
import io.smallrye.reactive.messaging.annotations.Emitter;
import io.smallrye.reactive.messaging.kafka.Record;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

@ApplicationScoped
public class KafkaProduceService implements ProducerService{
	
	private final Logger logger = Logger.getLogger(KafkaProduceService.class);
	
	@Inject @Channel("req-out")
    Emitter<Record<String, String>> emitter;
	
	@Override
	public void send(String key, String value) {
		logger.infof("sending :" + key + " " + value);
		emitter.send(Record.of(key, value));

	}	
}
