package com.rbac.service;

public interface ProducerService {
	void send(String key, String value);
}
