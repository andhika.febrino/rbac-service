package com.rbac.service;

import java.io.InputStream;

public interface FileUploadParseService {
	String parseFile(InputStream file);
}
