package com.rbac.controller;

import java.io.IOException;
import java.util.UUID;

import org.jboss.logging.Logger;
import org.jboss.resteasy.reactive.MultipartForm;

import com.rbac.service.FileUploadParseService;
import com.rbac.service.KafkaProduceService;
import com.rbac.service.ProducerService;
import com.rbac.service.UploadCSVParseService;
import com.rbac.service.UploadXLSParseService;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriBuilder;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.MULTIPART_FORM_DATA)
public class FileUploadController {
	
	private final Logger logger = Logger.getLogger(FileUploadController.class);
	
	private FileUploadParseService csvuploadService;
	private FileUploadParseService xlsuploadService;
	private ProducerService producerService;
	FileUploadController(UploadCSVParseService uploadCSVParseService,
			UploadXLSParseService uploadXLSParseService,
			KafkaProduceService kafkaProduceService){
		this.csvuploadService = uploadCSVParseService;
		this.xlsuploadService = uploadXLSParseService;
		this.producerService = kafkaProduceService;
	}
	
	/*
	 * @GET
	 * 
	 * @Path("") public String getIndex(){ return "index.html"; }
	 */
	
	@POST
    @Path("/upload")	
    public Response uploadFile(@MultipartForm FileUpload file,
    		@FormParam("fileInfo") String fileInfo) throws IOException{
		String uniqueId = UUID.randomUUID().toString();
		String result = null;
		if(fileInfo.endsWith(".csv")) {
    		result = csvuploadService.parseFile(file.getFile());    		
    	}else if(fileInfo.endsWith(".xls")) {
    		result = xlsuploadService.parseFile(file.getFile());
    	}else{
    		logger.infof("Unknown file type");
    	}
		
		if(result != null) {
			producerService.send(uniqueId, result);
		}
		UriBuilder uriBuilder = UriBuilder.fromPath("/");
		uriBuilder.queryParam("message", "File uploaded successfully!");
		return Response.seeOther(uriBuilder.build()).build();
    	
    }
	
}
