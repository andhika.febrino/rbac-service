# rbac-service

This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```shell script
./mvnw compile quarkus:dev

To test, open http://localhost:8080 and select file to upload, csv and excel format example is 
permission-xls.xls
permisson-csv.csv

```

NOTE:
Requires an active kafka platform, configure application.properties to point to kafka platform

